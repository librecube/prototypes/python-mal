Python-Mal
==========

Implementation of the CCSDS Message Abstraction Layer (MAL) in Python3.

Getting Started
---------------

Clone the repository and prepare a virtual environment for Python3.

.. code:: bash

  git clone https://gitlab.com/librecube/prototypes/python-mal
  cd python-mal
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -r requirements.txt
  pip install -e .

Examples
--------

The examples folder contains demo implementations of MO services. Typically
there is a *service.py* that contains the demo service, and a *consumer_app.py*
and *provider_app.py* file. First start the provider and then one or more
consumers, like so:

.. code:: bash

  (venv)$ python provider_app.py
  (venv)$ python consumer_app.py
