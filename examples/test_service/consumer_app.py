from mal import MalConsumer
from mal.structures import SessionType, QosLevel, Uri, Blob, List, Identifier,\
    String
from mal.access_control import NullSecurityManager
from mal.encoding import BinaryEncoding
from mal.transport import ZmtpTransport

from service import TestService


binding = MalConsumer(
    uri_from=Uri('malzmtp://127.0.0.1:1234/TestService'),
    authentication_id=Blob('SC1'),
    uri_to=Uri('malzmtp://127.0.0.1:2345/TestServiceConsumer'),
    qos_level=QosLevel.BEST_EFFORT,
    domain=List(Identifier('MissionA'), Identifier('SatelliteY')),
    network_zone=Identifier('GROUND'),
    session_type=SessionType.LIVE,
    session_name=Identifier('LIVE'),

    access_control=NullSecurityManager,
    transport=ZmtpTransport,
    encoding=BinaryEncoding,
)

test_service = TestService(binding)
test_service.test_send(String("Hello world!"), String("Hello world?"))
test_service.test_submit(String("Submit to world!"))
