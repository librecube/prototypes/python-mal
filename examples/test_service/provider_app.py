"""Just a simple ZMQ server that prints any receive ZMQ message on screen."""
import zmq

context = zmq.Context()
socket = context.socket(zmq.ROUTER)
socket.bind("tcp://*:2345")

try:
    while True:
        #  Wait for next request from client
        message = socket.recv_multipart()
        print("Received: %s" % message)

except KeyboardInterrupt:
    socket.close()
