from mal import SendInteractionPattern, SubmitInteractionPattern
from mal.structures import Identifier, UShort, UOctet, Boolean


class TestService():
    AREA_IDENTIFIER = Identifier('TEST')
    SERVICE_IDENTIFIER = Identifier('TestService')
    AREA_NUMBER = UShort(99)
    SERVICE_NUMBER = UShort(88)
    AREA_VERSION = UOctet(2)

    def __init__(self, binding):
        self.binding = binding

    def test_send(self, *body):
        interaction_pattern = SendInteractionPattern(
            self,
            operation_identifier=Identifier('testSend'),
            operation_number=UShort(1),
            support_in_replay=Boolean(False),
            capability_set=UShort(1)
        )
        return interaction_pattern(body)

    def test_submit(self, *body):
        interaction_pattern = SubmitInteractionPattern(
            self,
            operation_identifier=Identifier('testSubmit'),
            operation_number=UShort(2),
            support_in_replay=Boolean(False),
            capability_set=UShort(1)
        )
        return interaction_pattern(body)
