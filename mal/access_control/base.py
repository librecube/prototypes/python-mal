

class MalAccessControl:

    def __init__(self, binding, *args, **kwargs):
        raise NotImplementedError

    def check(self, message):
        raise NotImplementedError
