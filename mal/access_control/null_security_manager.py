from .base import MalAccessControl


class NullSecurityManager(MalAccessControl):

    def __init__(self, binding):
        self.binding = binding

    def check(self, message):
        return message
