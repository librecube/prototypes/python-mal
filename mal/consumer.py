from .core import Mal


class MalConsumer:

    def __init__(
            self,
            uri_from,
            authentication_id,
            uri_to,
            qos_level,
            domain,
            network_zone,
            session_type,
            session_name,
            broker_uri=None,
            qos_props=None,
            priority=None,
            access_control=None, transport=None, encoding=None):
        self.uri_from = uri_from
        self.authentication_id = authentication_id
        self.uri_to = uri_to
        self.qos_level = qos_level
        self.domain = domain
        self.network_zone = network_zone
        self.session_type = session_type
        self.session_name = session_name
        self.broker_uri = broker_uri
        self.qos_props = qos_props
        self.priority = priority

        self.access_control = access_control(self) if access_control else None
        self.transport = transport(self) if transport else None
        self.encoding = encoding(self) if encoding else None
        self.mal = Mal(self)
