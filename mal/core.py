from .structures import Long


class Mal:

    def __init__(self, binding):
        self.binding = binding
        self._transaction_id = 0

    def get_new_transaction_id(self):
        self._transaction_id += 1
        return Long(self._transaction_id)

    def submit(self, mal_message):
        # TODO: pre-check message
        # ...

        # access control
        mal_message = self.binding.access_control.check(mal_message)

        # TODO: store message transaction details
        # ...

        # pass message to transport layer
        self.binding.transport.transmit_request(mal_message)
