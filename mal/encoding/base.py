

class BaseEncoding:

    def __init__(self, binding, *args, **kwargs):
        raise NotImplementedError

    def encode_blob(self, value):
        raise NotImplementedError

    def encode_nullable_blob(self, value):
        raise NotImplementedError

    def encode_boolean(self, value):
        raise NotImplementedError

    def encode_nullable_boolean(self, value):
        raise NotImplementedError

    def encode_duration(self, value):
        raise NotImplementedError

    def encode_nullable_duration(self, value):
        raise NotImplementedError

    def encode_float(self, value):
        raise NotImplementedError

    def encode_nullable_float(self, value):
        raise NotImplementedError

    def encode_double(self, value):
        raise NotImplementedError

    def encode_nullable_double(self, value):
        raise NotImplementedError

    def encode_identifier(self, value):
        raise NotImplementedError

    def encode_nullable_identifier(self, value):
        raise NotImplementedError

    def encode_uri(self, value):
        raise NotImplementedError

    def encode_nullable_uri(self, value):
        raise NotImplementedError

    def encode_octet(self, value):
        raise NotImplementedError

    def encode_nullable_octet(self, value):
        raise NotImplementedError

    def encode_uoctet(self, value):
        raise NotImplementedError

    def encode_nullable_uoctet(self, value):
        raise NotImplementedError

    def encode_short(self, value):
        raise NotImplementedError

    def encode_nullable_short(self, value):
        raise NotImplementedError

    def encode_integer(self, value):
        raise NotImplementedError

    def encode_nullable_integer(self, value):
        raise NotImplementedError

    def encode_uinteger(self, value):
        raise NotImplementedError

    def encode_nullable_uinteger(self, value):
        raise NotImplementedError

    def encode_long(self, value):
        raise NotImplementedError

    def encode_nullable_long(self, value):
        raise NotImplementedError

    def encode_ulong(self, value):
        raise NotImplementedError

    def encode_nullable_ulong(self, value):
        raise NotImplementedError

    def encode_string(self, value):
        raise NotImplementedError

    def encode_nullable_string(self, value):
        raise NotImplementedError

    def encode_time(self, value):
        raise NotImplementedError

    def encode_nullable_time(self, value):
        raise NotImplementedError

    def encode_finetime(self, value):
        raise NotImplementedError

    def encode_nullable_finetime(self, value):
        raise NotImplementedError

    def encode_element(self, value):
        raise NotImplementedError

    def encode_nullable_element(self, value):
        raise NotImplementedError

    def encode_attribute(self, value):
        raise NotImplementedError

    def encode_nullable_attribute(self, value):
        raise NotImplementedError
