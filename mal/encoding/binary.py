from .base import BaseEncoding


class BinaryEncoding(BaseEncoding):

    def __init__(self, binding, varint_supported=False):
        self.binding = binding
        self.varint_supported = varint_supported

    def encode_identifier(self, Identifier):
        return self.encode_string(Identifier)

    def encode_uri(self, Uri):
        return self.encode_string(Uri)

    def encode_boolean(self, Boolean):
        encoded = bytearray()
        if Boolean.value is True:
            encoded.append(1)
        else:
            encoded.append(0)
        return encoded

    def encode_octet(self, Octet):
        encoded = bytearray()
        encoded.append(Octet.value)
        return encoded

    def encode_uoctet(self, UOctet):
        return self.encode_octet(UOctet)

    def encode_short(self, Short):
        encoded = bytearray()
        if self.varint_supported:
            raise NotImplementedError
        else:
            encoded.append(Short.value >> 8)
            encoded.append(Short.value & 0xff)
        return encoded

    def encode_ushort(self, UShort):
        return self.encode_short(UShort)

    def encode_integer(self, Integer):
        encoded = bytearray()
        if self.varint_supported:
            raise NotImplementedError
        else:
            encoded.append((Integer.value >> 24) & 0xff)
            encoded.append((Integer.value >> 16) & 0xff)
            encoded.append((Integer.value >> 8) & 0xff)
            encoded.append(Integer.value & 0xff)
        return encoded

    def encode_uinteger(self, UInteger):
        return self.encode_integer(UInteger)

    def encode_long(self, Long):
        encoded = bytearray()
        if self.varint_supported:
            raise NotImplementedError
        else:
            encoded.append((Long.value >> 56) & 0xff)
            encoded.append((Long.value >> 48) & 0xff)
            encoded.append((Long.value >> 40) & 0xff)
            encoded.append((Long.value >> 32) & 0xff)
            encoded.append((Long.value >> 24) & 0xff)
            encoded.append((Long.value >> 16) & 0xff)
            encoded.append((Long.value >> 8) & 0xff)
            encoded.append(Long.value & 0xff)
        return encoded

    def encode_ulong(self, ULong):
        return self.encode_long(ULong)

    def encode_string(self, String):
        encoded = bytearray()
        encoded.append(len(String.value))
        encoded.extend(bytearray(String.value, 'utf-8'))
        return encoded
