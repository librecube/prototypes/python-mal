

class MalMessage:

    def __init__(self, header, body, qos_properties=None):
        self.header = header
        self.body = body
        self.qos_properties = qos_properties


class MalMessageHeader:

    def __init__(
            self,
            uri_from,
            authentication_id,
            uri_to,
            timestamp,
            qos_level,
            priority,
            domain,
            network_zone,
            session_type,
            session_name,
            interaction_type,
            interaction_stage,
            transaction_id,
            service_area,
            service,
            operation,
            area_version,
            is_error_message):
        self.uri_from = uri_from
        self.authentication_id = authentication_id
        self.uri_to = uri_to
        self.timestamp = timestamp
        self.qos_level = qos_level
        self.priority = priority
        self.domain = domain
        self.network_zone = network_zone
        self.session_type = session_type
        self.session_name = session_name
        self.interaction_type = interaction_type
        self.interaction_stage = interaction_stage
        self.transaction_id = transaction_id
        self.service_area = service_area
        self.service = service
        self.operation = operation
        self.area_version = area_version
        self.is_error_message = is_error_message


class MalMessageBody:

    def __init__(self, elements):
        self.elements = elements

    def get_element_count(self):
        return len(self.elements)

    def get_encoded_body(self, encoding):
        encoded_body = bytearray()
        for element in self.elements:
            encoded_body.extend(element.encode(encoding))
        return encoded_body
