from .message import MalMessage, MalMessageHeader, MalMessageBody
from mal.structures import Boolean, Time
from mal.structures import InteractionType, InteractionStage


INITIATED = "initiated"
FINAL = "final"


class SendInteractionPattern:

    def __init__(
            self, service, operation_identifier, operation_number,
            support_in_replay, capability_set):
        self.service = service
        self.operation_identifier = operation_identifier
        self.operation_number = operation_number
        self.support_in_replay = support_in_replay
        self.capability_set = capability_set

    def __call__(self, body):
        self.transaction_id = self.service.binding.mal.get_new_transaction_id()
        self.send_request(body)
        return self  # return instance of itself

    def send_request(self, body):
        # build MAL message
        mal_header = MalMessageHeader(
            uri_from=self.service.binding.uri_from,
            authentication_id=self.service.binding.authentication_id,
            uri_to=self.service.binding.uri_to,
            timestamp=Time(0),
            qos_level=self.service.binding.qos_level,
            priority=self.service.binding.priority,
            domain=self.service.binding.domain,
            network_zone=self.service.binding.network_zone,
            session_type=self.service.binding.session_type,
            session_name=self.service.binding.session_name,
            interaction_type=InteractionType.SEND,
            interaction_stage=InteractionStage.SEND,
            transaction_id=self.transaction_id,
            service_area=self.service.AREA_NUMBER,
            service=self.service.SERVICE_NUMBER,
            operation=self.operation_number,
            area_version=self.service.AREA_VERSION,
            is_error_message=Boolean(False))
        mal_body = MalMessageBody(body)
        mal_message = MalMessage(mal_header, mal_body)

        # TODO: test for error condition and pass to application layer
        # ...
        self.service.binding.mal.submit(mal_message)


class SubmitInteractionPattern:

    def __init__(
            self, service, operation_identifier, operation_number,
            support_in_replay, capability_set):
        self.service = service
        self.operation_identifier = operation_identifier
        self.operation_number = operation_number
        self.support_in_replay = support_in_replay
        self.capability_set = capability_set

    def __call__(self, body):
        self.transaction_id = self.service.binding.mal.get_new_transaction_id()
        self.state = INITIATED
        self.submit_request(body)
        return self  # return instance of itself

    def submit_request(self, body):
        # build MAL message
        mal_header = MalMessageHeader(
            uri_from=self.service.binding.uri_from,
            authentication_id=self.service.binding.authentication_id,
            uri_to=self.service.binding.uri_to,
            timestamp=Time(0),
            qos_level=self.service.binding.qos_level,
            priority=self.service.binding.priority,
            domain=self.service.binding.domain,
            network_zone=self.service.binding.network_zone,
            session_type=self.service.binding.session_type,
            session_name=self.service.binding.session_name,
            interaction_type=InteractionType.SUBMIT,
            interaction_stage=InteractionStage.SUBMIT,
            transaction_id=self.transaction_id,
            service_area=self.service.AREA_NUMBER,
            service=self.service.SERVICE_NUMBER,
            operation=self.operation_number,
            area_version=self.service.AREA_VERSION,
            is_error_message=Boolean(False))
        mal_body = MalMessageBody(body)
        mal_message = MalMessage(mal_header, mal_body)

        # TODO: test for error condition and pass to application layer
        # ...
        self.service.binding.mal.submit(mal_message)

    def ack_indication(self):
        self.state = FINAL
