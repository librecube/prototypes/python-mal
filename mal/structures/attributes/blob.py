import copy

from mal.structures import Attribute


class Blob(Attribute):

    def __init__(self, value=None, offset=0, length=0, source_url=None):
        self.short_form = Attribute.BLOB_SHORT_FORM
        self.type_short_form = Attribute.BLOB_TYPE_SHORT_FORM
        self.value = value
        self.offset = offset
        self.length = length
        self.source_url = source_url

        if value is not None:
            self.source_url = None
        elif source_url is not None:
            self.value = None
            self.offset = 0
            self.length = 0

    def encode(self, encoder):
        encoder.encode_blob(self)

    def decode(self, decoder):
        return decoder.decode_blob()

    def is_url_based(self):
        return self.source_url is not None

    def detach(self):
        self.source_url = None

    def delete(self):
        del self.value

    def get_value(self):
        if self.is_url_based():
            raise NotImplementedError
            # TODO...
        return copy.deepcopy(self.value)

    def get_offset(self):
        return self.offset

    def get_length(self):
        return self.length

    def get_url(self):
        return self.source_url
