from mal import MalException
from mal.structures import Attribute


class Boolean(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.BOOLEAN_SHORT_FORM
        self.type_short_form = Attribute.BOOLEAN_TYPE_SHORT_FORM
        if not isinstance(value, bool):
            raise MalException()
        self.value = value

    def encode(self, encoding):
        encoding.encode_boolean(self)

    def decode(self, decoding):
        return decoding.decode_boolean()
