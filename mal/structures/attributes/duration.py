from mal.structures import Attribute


class Duration(Attribute):

    def __init__(self, value=None):
        self.short_form = Attribute.DURATION_SHORT_FORM
        self.type_short_form = Attribute.DURATION_TYPE_SHORT_FORM
        if value:
            self.value = value
        else:
            self.value = 0

    def encode(self, encoder):
        encoder.encode_duration(self)

    def decode(self, decoder):
        return decoder.decode_duration()
