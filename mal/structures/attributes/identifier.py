from mal.structures import Attribute


class Identifier(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.IDENTIFIER_SHORT_FORM
        self.type_short_form = Attribute.IDENTIFIER_TYPE_SHORT_FORM
        self.value = value

    def encode(self, encoder):
        encoder.encode_identifier(self)

    def decode(self, decoder):
        return decoder.decode_identifier()
