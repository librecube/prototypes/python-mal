from mal import MalException
from mal.structures import Attribute
from .identifier import Identifier


class List(Attribute):

    def __init__(self, *list_items):
        attribute = list_items[0]
        if isinstance(attribute, Identifier):
            self.short_form = -Attribute.IDENTIFIER_SHORT_FORM
            self.type_short_form = -Attribute.IDENTIFIER_TYPE_SHORT_FORM

            def encode(self, encoder):
                encoder.encode_identifier_list(self)
            self.encode = encode
        # TODO: elif ...

    def encode(self, encoder):
        encoder.encode_long(self)

    def decode(self, decoder):
        raise NotImplementedError
