from mal import MalException
from mal.structures import Attribute


class Octet(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.OCTET_SHORT_FORM
        self.type_short_form = Attribute.OCTET_TYPE_SHORT_FORM
        if not isinstance(value, int):
            raise MalException()
        if value < -128 or value > 127:
            raise MalException()
        self.value = value

    def encode(self, encoder):
        encoder.encode_octet(self)

    def decode(self, decoder):
        return decoder.decode_octet()
