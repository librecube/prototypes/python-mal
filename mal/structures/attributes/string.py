from mal.structures import Attribute


class String(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.STRING_SHORT_FORM
        self.type_short_form = Attribute.STRING_TYPE_SHORT_FORM
        self.value = value

    def encode(self, encoding):
        return encoding.encode_string(self)

    def decode(self, decoding):
        return decoding.decode_string()
