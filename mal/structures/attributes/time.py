from mal.structures import Attribute


class Time(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.TIME_SHORT_FORM
        self.type_short_form = Attribute.TIME_TYPE_SHORT_FORM
        self.value = value

    def encode(self, encoder):
        encoder.encode_time(self)

    def decode(self, decoder):
        return decoder.decode_time()
