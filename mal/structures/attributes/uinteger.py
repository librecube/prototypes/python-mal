from mal import MalException
from mal.structures import Attribute


class UInteger(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.UINTEGER_SHORT_FORM
        self.type_short_form = Attribute.UINTEGER_TYPE_SHORT_FORM
        if not isinstance(value, int):
            raise MalException()
        if value < 0 or value > 4294967295:
            raise MalException()
        self.value = value

    def encode(self, encoder):
        encoder.encode_uinteger(self)

    def decode(self, decoder):
        return decoder.decode_uinteger()
