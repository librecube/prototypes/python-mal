from mal import MalException
from mal.structures import Attribute


class UOctet(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.UOCTET_SHORT_FORM
        self.type_short_form = Attribute.UOCTET_TYPE_SHORT_FORM
        if not isinstance(value, int):
            raise MalException()
        if value < 0 or value > 255:
            raise MalException()
        self.value = value

    def encode(self, encoder):
        encoder.encode_uoctet(self)

    def decode(self, decoder):
        return decoder.decode_uoctet()
