from mal.structures import Attribute


class Uri(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.URI_SHORT_FORM
        self.type_short_form = Attribute.URI_TYPE_SHORT_FORM
        self.value = value

    def encode(self, encoder):
        encoder.encode_uri(self)

    def decode(self, decoder):
        return decoder.decode_uri()
