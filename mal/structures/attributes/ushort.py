from mal import MalException
from mal.structures import Attribute


class UShort(Attribute):

    def __init__(self, value):
        self.short_form = Attribute.USHORT_SHORT_FORM
        self.type_short_form = Attribute.USHORT_TYPE_SHORT_FORM
        if not isinstance(value, int):
            raise MalException()
        if value < 0 or value > 65535:
            raise MalException()
        self.value = value

    def encode(self, encoder):
        encoder.encode_ushort(self)

    def decode(self, decoder):
        return decoder.decode_ushort()
