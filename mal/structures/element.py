

class Element:

    @classmethod
    def create_element(cls):
        return cls()

    def get_short_form(self):
        raise NotImplementedError

    def get_type_short_form(self):
        raise NotImplementedError

    def get_area_number(self):
        raise NotImplementedError

    def get_area_version(self):
        raise NotImplementedError

    def get_service_number(self):
        raise NotImplementedError

    def encode(self, encoder):
        raise NotImplementedError

    def decode(self, decoder):
        raise NotImplementedError

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return True
