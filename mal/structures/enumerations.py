from enum import Enum


class InteractionType(Enum):
    SEND = 1
    SUBMIT = 2
    REQUEST = 3
    INVOKE = 4
    PROGESS = 5
    PUBSUB = 6


class InteractionStage(Enum):
    SEND = None
    SUBMIT = 1
    ACK = 2
    ERROR = 2


class QosLevel(Enum):
    BEST_EFFORT = 1
    ASSURED = 2
    QUEUED = 3
    TIMELY = 4


class SessionType(Enum):
    LIVE = 1
    SIMULATION = 2
    REPLAY = 3


class UpdateType(Enum):
    CREATION = 1
    UPDATE = 2
    MODIFICATION = 3
    DELETION = 4
