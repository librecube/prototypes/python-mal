

class BaseTransport:

    def __init__(self, binding, *args, **kwargs):
        raise NotImplementedError

    def transmit_request(self, mal_message, qos_properties=None):
        raise NotImplementedError
