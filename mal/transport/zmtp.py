from urllib.parse import urlparse
import zmq
from mal import MalException, MalConsumer
from mal.structures import InteractionType, InteractionStage


ZMTP_BINDING_VERSION_NUMBER = 1
ENCODING_FIXED_BINARY = 0
ENCODING_VARIABLE_BINARY = 1
ENCODING_SPLIT_BINARY = 2
ENCODING_OTHER = 3
COMMUNICATION_PATTERN_P2P = 1  # mandatory
COMMUNICATION_PATTERN_MULTICAST = 2  # optional


def get_sdu_type(interaction_type, interaction_stage):
    if interaction_type == InteractionType.SEND:
        return 0
    elif interaction_type == InteractionType.SUBMIT:
        if interaction_stage == InteractionStage.SUBMIT:
            return 1
        elif interaction_stage == InteractionStage.ACK or\
                interaction_stage == InteractionStage.ERROR:
            return 2
    else:
        raise NotImplementedError


def get_local_ptp_zmtp_uri(self, mal_uri):
    uri = urlparse(mal_uri)
    if uri.port is None:
        raise MalException
    local_uri = "tcp://*:" + str(uri.port)
    return local_uri


def get_remote_ptp_zmtp_uri(mal_uri):
    uri = urlparse(mal_uri)
    if uri.port is None:
        raise MalException
    remote_uri = "tcp://" + uri.hostname + ":" + str(uri.port)
    return remote_uri


def get_random_port(self):
    import socket
    from contextlib import closing
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        return s.getsockname()[1]


def get_default_host(self):
    import socket
    hostname = socket.gethostname()
    return socket.gethostbyaddr(hostname)[2][0]


def encode_varint(number):
    buf = b''
    while True:
        towrite = number & 0x7f
        number >>= 7
        if number:
            buf += bytes((towrite | 0x80,))
        else:
            buf += bytes((towrite,))
            break
    return buf


class ZmtpTransport:

    def __init__(self, binding):
        self.binding = binding
        self.zmq_context = zmq.Context()

        if isinstance(binding, MalConsumer):
            self.zmtp_uri = get_remote_ptp_zmtp_uri(self.binding.uri_to.value)
            self.as_server = False
        else:
            # TODO...
            raise NotImplementedError

        self.socket = self.zmtp_open(
            COMMUNICATION_PATTERN_P2P,
            self.zmtp_uri,
            self.as_server)

    def transmit_request(self, mal_message, qos_properties=None):
        # TODO: pre-check message
        # ...
        mal_header = mal_message.header
        mal_body = mal_message.body

        zmtp_header = self.get_zmtp_header(mal_header)
        zmtp_body = mal_body.get_encoded_body(self.binding.encoding)

        pdu = bytearray()
        pdu.extend(zmtp_header)
        pdu.extend(zmtp_body)
        self.zmtp_send(self.socket, pdu)

    def get_zmtp_header(self, mal_header):
        version_number = 1
        sdu_type = get_sdu_type(
            mal_header.interaction_type, mal_header.interaction_stage)
        service_area = mal_header.service_area
        service = mal_header.service
        operation = mal_header.operation
        area_version = mal_header.area_version
        is_error_message = mal_header.is_error_message
        qos_level = mal_header.qos_level
        session = mal_header.session_type
        transaction_id = mal_header.transaction_id
        encoding_id_flag = ENCODING_FIXED_BINARY
        priority_flag = False
        timestamp_flag = False
        network_zone_flag = False
        session_name_flag = False
        domain_flag = False
        authentication_id_flag = False
        uri_from = mal_header.uri_from
        uri_to = mal_header.uri_to
        # extended_encoding_id = None
        # priority = mal_header.priority
        # timestamp = 0
        # network_zone = mal_header.network_zone
        # session_name = mal_header.session_name
        # domain = mal_header.domain
        # authentication_id = mal_header.authentication_id

        pdu = bytearray()
        pdu.append(
            (version_number << 5) +
            sdu_type
        )

        # area, service, operation, version
        pdu.append(service_area.value >> 8)
        pdu.append(service_area.value & 0xff)
        pdu.append(service.value >> 8)
        pdu.append(service.value & 0xff)
        pdu.append(operation.value >> 8)
        pdu.append(operation.value & 0xff)
        pdu.append(area_version.value)

        # error message flag, qos, session
        pdu.append(
            (int(is_error_message.value) << 7) +
            (qos_level.value << 4) +
            session.value
        )

        # transaction id
        pdu.append((transaction_id.value >> 56) & 0xff)
        pdu.append((transaction_id.value >> 48) & 0xff)
        pdu.append((transaction_id.value >> 40) & 0xff)
        pdu.append((transaction_id.value >> 32) & 0xff)
        pdu.append((transaction_id.value >> 24) & 0xff)
        pdu.append((transaction_id.value >> 16) & 0xff)
        pdu.append((transaction_id.value >> 8) & 0xff)
        pdu.append(transaction_id.value & 0xff)

        # flags
        pdu.append(
            (int(encoding_id_flag) << 6) +
            (int(priority_flag) << 5) +
            (int(timestamp_flag) << 4) +
            (int(network_zone_flag) << 3) +
            (int(session_name_flag) << 2) +
            (int(domain_flag) << 1) +
            (int(authentication_id_flag))
        )

        # uri_from, not using mdk
        str_utf8 = uri_from.value.encode("utf-8")
        length = len(str_utf8)
        pdu.extend(encode_varint(length))
        for i in range(length):
            pdu.append(str_utf8[i])

        # uri_to, not using mdk
        str_utf8 = uri_to.value.encode("utf-8")
        length = len(str_utf8)
        pdu.extend(encode_varint(length))
        for i in range(length):
            pdu.append(str_utf8[i])

        return pdu

    def zmtp_open(self, communication_pattern, zmtp_uri, as_server):
        if as_server:
            if communication_pattern == COMMUNICATION_PATTERN_P2P:
                socket = self.zmq_context.socket(zmq.ROUTER)
            elif communication_pattern == COMMUNICATION_PATTERN_MULTICAST:
                socket = self.zmq_context.socket(zmq.SUB)
                # socket.subscribe("")
                raise NotImplementedError
        else:
            if communication_pattern == COMMUNICATION_PATTERN_P2P:
                socket = self.zmq_context.socket(zmq.DEALER)
            elif communication_pattern == COMMUNICATION_PATTERN_MULTICAST:
                socket = self.zmq_context.socket(zmq.PUB)
        socket.connect(zmtp_uri)
        return socket

    def zmtp_send(self, socket, frame):
        print("Socket: {}, Frame: {}".format(socket, frame))
        socket.send(frame)

    def zmtp_receive(self, socket):
        return socket.recv()
