from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='python-mal',
    version='0.0.1',
    description='Python implementation of Message Abstraction Layer',
    long_description=long_description,
    url='https://gitlab.com/librecube/prototypes/python-mal',
    author='Artur Scholz',
    author_email='artur.scholz@librecube.org',
    license='GPL',
    python_requires='>=3',
    keywords='ccsds mal',
    packages=['mal'],
    install_requires=[],
)
